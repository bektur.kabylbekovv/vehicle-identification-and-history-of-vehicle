package com.example.carproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CarProjectApplication {
	public static void main(String[] args) {
		SpringApplication.run(CarProjectApplication.class, args);
	}
}
