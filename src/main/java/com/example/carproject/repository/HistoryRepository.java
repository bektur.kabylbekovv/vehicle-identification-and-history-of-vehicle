package com.example.carproject.repository;

import com.example.carproject.model.History;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoryRepository extends JpaRepository<History, Long> {
    List<History> findAll();
    List<History> findAllByVin(String vin);
    Boolean existsByVin(String vin);
}