package com.example.carproject.repository;

import com.example.carproject.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    Car findCarByVin(String vin);
    Boolean existsCarsByVin(String vin);
}