package com.example.carproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class VinAlreadyExistsException extends RuntimeException {
    private String mark;
    private String model;
    private String carVin;

    public VinAlreadyExistsException( String mark, String model, String carVin) {
        super(String.format("%s %s is already exists with vin num %s", mark, model, carVin));
        this.mark = mark;
        this.model = model;
        this.carVin = carVin;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCarVin() {
        return carVin;
    }

    public void setCarVin(String carVin) {
        this.carVin = carVin;
    }
}