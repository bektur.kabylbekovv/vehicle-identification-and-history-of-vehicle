package com.example.carproject.controller;

import com.example.carproject.exception.ResourceNotFoundException;
import com.example.carproject.exception.VinAlreadyExistsException;
import com.example.carproject.model.Car;
import com.example.carproject.model.History;
import com.example.carproject.repository.CarRepository;
import com.example.carproject.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class HistoryController {

    @Autowired
    HistoryRepository historyRepository;

    // Get All Cars
    @GetMapping("/history")
    public List<History> getAllHistory() {
        return historyRepository.findAll();
    }

    @GetMapping("/history/vincode/{vin}")
    public List<History> getHistoryByVinCode(@PathVariable(value = "vin") String vin){
        if (!historyRepository.existsByVin(vin)){
            throw new ResourceNotFoundException("Car","vin", vin);
        }
        return historyRepository.findAllByVin(vin);
    }

    // Create a new Car
    @PostMapping("/history")
    public History createHistory(@Valid @RequestBody History history) {
        return historyRepository.save(history);
    }

    // Get a Single car
    @GetMapping("/history/{id}")
    public History getHistoryById(@PathVariable(value = "id") Long historyId) {
        return historyRepository.findById(historyId)
                .orElseThrow(() -> new ResourceNotFoundException("History", "id", historyId));
    }

    // Delete a car
    @DeleteMapping("/history/{id}")
    public ResponseEntity<?> deleteHistory(@PathVariable(value = "id") Long historyId) {
        History history = historyRepository.findById(historyId)
                .orElseThrow(() -> new ResourceNotFoundException("History", "id", historyId));

        historyRepository.delete(history);

        return ResponseEntity.ok().build();
    }
}