package com.example.carproject.controller;

import com.example.carproject.exception.ResourceNotFoundException;
import com.example.carproject.exception.VinAlreadyExistsException;
import com.example.carproject.model.Car;
import com.example.carproject.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CarController {

    @Autowired
    CarRepository carRepository;

    // Get All Cars
    @GetMapping("/cars")
    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    @GetMapping("/cars/vincode/{vin}")
    public Car getCarByVinCode(@PathVariable(value = "vin") String vin){
        if (!carRepository.existsCarsByVin(vin)){
            throw new ResourceNotFoundException("Car","vin", vin);
        }
        return carRepository.findCarByVin(vin);
    }

    // Create a new Car
    @PostMapping("/cars")
    public Car createCar(@Valid @RequestBody Car car) {
        if (carRepository.existsCarsByVin(car.getVin())){
            throw new VinAlreadyExistsException(car.getMark(), car.getModel(), car.getVin());
        }
        return carRepository.save(car);
    }

    // Get a Single car
    @GetMapping("/cars/{id}")
    public Car getCarById(@PathVariable(value = "id") Long carId) {
        return carRepository.findById(carId)
                .orElseThrow(() -> new ResourceNotFoundException("Car", "id", carId));
    }

    // Delete a car
    @DeleteMapping("/cars/{id}")
    public ResponseEntity<?> deleteCar(@PathVariable(value = "id") Long carId) {
        Car car = carRepository.findById(carId)
                .orElseThrow(() -> new ResourceNotFoundException("Car", "id", carId));

        carRepository.delete(car);

        return ResponseEntity.ok().build();
    }

}